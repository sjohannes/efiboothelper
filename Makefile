MYPY = mypy
PYFLAKES = pyflakes
PYTHON = python3

APP = efiboothelper


check: pyflakes mypy

pyflakes:
	$(PYFLAKES) $(APP)

mypy:
	$(MYPY) --ignore-missing-imports $(APP)


.PHONY: check mypy pyflakes
