=============
efiboothelper
=============

Show information relevant to editing EFI boot entries.

efiboothelper shows various information that helps you build an efibootmgr command line, namely:

* List of existing boot entries, similar to the output of efibootmgr but in a format that is easier to read.
  You can press Ctrl+C to copy the content of the Parameters column.

* List of partitions, similar to the output of lsblk or blkid.
  You can press Ctrl+C to copy the content of the PARTUUID column.

* Manpage of efibootmgr, in a nice webpage format.


Requirements
============

* Python 3

* GObject Introspection bindings for:

  * GTK+ 3
  * WebKitGTK+ 2

* efibootmgr + manpage

* lsblk


Usage
=====

Run ``efiboothelper.sh``.

To install, run ``python3 setup.py install``.
