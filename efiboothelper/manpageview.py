# efiboothelper
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['ManpageView']


import subprocess
import tempfile
from typing import IO, Optional

import gi
gi.require_version('WebKit2', '4.0')
from gi.repository import Gio, WebKit2


CMD_MAN = ['/usr/bin/env', 'man']

# flake8: disable=W191
SCRIPT = rb'''
<script>//<![CDATA[
window.onload = () => {
	const $ = document.querySelector.bind(document);
	const $$ = document.querySelectorAll.bind(document);

	const body = document.body;
	body.style.fontFamily = 'monospace';
	body.style.lineHeight = '140%';

	// Make the TOC horizontal
	const toc = $('h1 + p');
	toc.style.textAlign = 'center';
	toc.querySelectorAll('br').forEach(br => br.outerHTML = ' ');

	// Fix excessive indentation
	// - Normal text
	$$('*[style*="margin-left:11%"]').forEach(e => e.style.marginLeft = '4%')
	$$('*[style*="margin-left:22%"]').forEach(e => e.style.marginLeft = '8%')
	// - List
	$$('col[width="11%"]').forEach(col => col.width = '4%');
	// - List bullet
	$$('col[width="12%"]').forEach(col => col.width = '2%');
	// - List body
	$$('col[width="75%"]').forEach(col => col.width = '');
}
//]]></script>
'''
# flake8: enable=W191


htmlfile: Optional[IO] = None


def create_manpage_html() -> IO:
    # We need to use a tempfile because internal links don't work on pages
    # loaded with WebView.load_bytes.
    global htmlfile
    if htmlfile:
        return htmlfile
    data = subprocess.check_output(CMD_MAN + ['-Txhtml', 'efibootmgr'])
    f = tempfile.NamedTemporaryFile(mode='wb', prefix='efiboothelper.', suffix='.xhtml')
    pos = data.find(b'</head>')
    f.write(data[:pos])
    f.write(SCRIPT)
    f.write(data[pos:])
    htmlfile = f
    return f


class ManpageView(WebKit2.WebView):
    def __init__(self):
        context = WebKit2.WebContext.new_ephemeral()
        context.set_cache_model(WebKit2.CacheModel.DOCUMENT_VIEWER)
        super().__init__(web_context=context)
        f = create_manpage_html()
        uri = Gio.File.new_for_path(f.name).get_uri()
        self.load_uri(uri)
