# efiboothelper
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['Application', 'Window', 'main']


from gettext import gettext as _

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from .bootview import BootView
from .manpageview import ManpageView
from .partitionview import PartitionView


class Window(Gtk.ApplicationWindow):
    def __init__(self, application):
        super().__init__(application=application, title='efiboothelper')
        self.set_icon_name('drive-harddisk')

        firstchild = vpaned1 = Gtk.Paned(orientation=Gtk.Orientation.VERTICAL, wide_handle=True)
        self.add(vpaned1)
        vpaned2 = Gtk.Paned(orientation=Gtk.Orientation.VERTICAL, wide_handle=True)
        vpaned1.pack1(vpaned2, True, True)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vpaned2.pack1(box, True, True)
        box.pack_start(Gtk.Label(label=_("Boot entries")), False, False, 0)
        bv = BootView()
        box.pack_start(bv, True, True, 0)
        self.add_accel_group(bv.accel_group)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vpaned2.pack2(box, True, True)
        box.pack_start(Gtk.Label(label=_("Partitions")), False, False, 0)
        pv = PartitionView()
        box.pack_start(pv, True, True, 0)
        self.add_accel_group(pv.accel_group)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vpaned1.pack2(box, True, True)
        box.pack_start(Gtk.Label(label=_("efibootmgr documentation")), False, False, 0)
        mv = ManpageView()
        box.pack_start(mv, True, True, 0)

        partuuid_map = {}
        for part in PartitionView.read_partitions():
            pv.append(part)
            partuuid_map[part.partuuid] = part.partition

        for be in BootView.read_boot_entries(partuuid_map):
            bv.append(be)

        ag = Gtk.AccelGroup()
        key, mods = Gtk.accelerator_parse('<Primary>W')
        ag.connect(key, mods, Gtk.AccelFlags.VISIBLE, lambda _ag, w, *_: w.destroy())
        key, mods = Gtk.accelerator_parse('<Primary>Q')
        ag.connect(key, mods, Gtk.AccelFlags.VISIBLE, lambda *_: application.quit())
        self.add_accel_group(ag)

        vpaned1.set_position(500)
        vpaned2.set_position(250)
        self.set_default_size(1000, 800)
        firstchild.show_all()


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='com.gitlab.sjohannes.efiboothelper')
    def do_activate(self):
        window = Window(self)
        window.show()


def main() -> None:
    import sys
    Application().run(sys.argv)

