# efiboothelper
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['Partition', 'PartitionView']


from gettext import gettext as _
import locale
import subprocess
from typing import Iterable, Iterator, Mapping, NamedTuple, Union

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


CMD_LSBLK = ['/usr/bin/env', 'lsblk']


class Partition(NamedTuple):
    partition: str
    fstype: str
    mountpoint: str
    size: int
    description: str
    partuuid: str


def parse_lsblk() -> Iterator[Partition]:
    import json
    data = subprocess.check_output(CMD_LSBLK + ['--bytes', '--json',
        '--output', 'name,model,size,fstype,mountpoint,label,partlabel,partuuid,partflags'])
    js = json.loads(data)

    from typing import Any, cast
    # The Any below should be 'Node', but mypy doesn't support recursive types yet;
    # see https://github.com/python/mypy/issues/731
    Node = Mapping[str, Union[str, int, None, Iterable[Any]]]
    def parse(devs: Iterable[Node]):
        for dev in devs:
            partition = dev['name']
            fstype = dev['fstype'] or ''
            mountpoint = dev['mountpoint'] or ''
            size = dev['size'] or 0
            if isinstance(size, str):  # Old lsblk
                size = int(size)
            partuuid = dev['partuuid'] or ''
            assert isinstance(partition, str)
            assert isinstance(fstype, str)
            assert isinstance(mountpoint, str)
            assert isinstance(size, int)
            assert isinstance(partuuid, str)

            model = dev['model']
            label = dev['label']
            partlabel = dev['partlabel']
            desc = []
            if label:
                assert isinstance(label, str)
                desc.append(label.strip())
            if partlabel:
                assert isinstance(partlabel, str)
                desc.append(partlabel.strip())
            if model:
                assert isinstance(model, str)
                desc.append(model.strip())
            if desc:
                description = _(" / ").join(desc)
            else:
                description = ''

            yield Partition(partition=partition, fstype=fstype, mountpoint=mountpoint, size=size, description=description, partuuid=partuuid)
            children = dev.get('children')
            if children:
                children = cast(Iterable[Node], children)
                yield from parse(children)
    yield from parse(js['blockdevices'])


class PartitionView(Gtk.ScrolledWindow):
    def __init__(self):
        super().__init__()

        columns = [_("Device"), _("Filesystem"), _("Mountpoint"), _("Size"), _("Description"), _("PARTUUID")]

        self.model = model = Gtk.ListStore(*([str] * len(columns)))

        tv = Gtk.TreeView(model=model)
        self.add(tv)
        tv.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        for i, column in enumerate(columns):
            tv.insert_column_with_attributes(-1, column, Gtk.CellRendererText(), text=i)

        tv.show_all()

        self.accel_group = ag = Gtk.AccelGroup()
        def copy(_ag, window, *_):
            if window.get_focus() != tv:
                return False
            model, it = tv.get_selection().get_selected()
            params = model[it][-1]
            from gi.repository import Gdk
            clipboard = Gtk.Clipboard.get_default(Gdk.Display.get_default())
            clipboard.set_text(params, len(params))
            return True
        key, mods = Gtk.accelerator_parse('<Primary>C')
        ag.connect(key, mods, Gtk.AccelFlags.VISIBLE, copy)

    @staticmethod
    def read_partitions() -> Iterator[Partition]:
        yield from parse_lsblk()

    def append(self, partition: Partition) -> None:
        size = locale.format('%.2f', partition.size / 1000_000_000, grouping=True)
        size = _("{size} GB").format(size=size)
        self.model.append([partition.partition, partition.fstype, partition.mountpoint, size, partition.description, partition.partuuid])
