# efiboothelper
# Copyright (C) 2018  Johannes Sasongko <sasongko@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


__all__ = ['BootEntry', 'BootView']


from gettext import gettext as _
import re
import struct
import subprocess
from typing import Iterator, Mapping, NamedTuple, Optional

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


CMD_EFIBOOTMGR = ['/usr/bin/env', 'efibootmgr']
FMT_BOOTXXXX_PATH = '/sys/firmware/efi/efivars/{}-8be4df61-93ca-11d2-aa0d-00e098032b8c'
RE_BOOT_ENTRY = re.compile('^(Boot[0-9A-F]{4})[* ] .*?\t.*$')


class BootEntry(NamedTuple):
    bootnum: str
    name: str
    partition: str
    path: str
    parameters: str


def parse_efibootmgr(partuuid_map: Mapping[str, str]) -> Iterator[BootEntry]:
    data = subprocess.check_output(CMD_EFIBOOTMGR + ['-v'])
    for line_b in data.split(b'\n'):
        line = line_b.decode('ascii', errors='replace')
        m = RE_BOOT_ENTRY.match(line)
        if not m:
            continue
        bootvar = m.group(1)
        bootnum = bootvar[4:]
        line = line.split(' ', 1)[1]
        name, line = line.split('\t', 1)
        p = line.index(')')
        partition = line[:p+1]
        parameters = line[p+1:]
        path = ''
        if partition.startswith('HD('):
            partition = partition.split(',')[2]
            partition = partuuid_map.get(partition, partition)
        if parameters.startswith('/File('):
            p = parameters.index(')')
            path = parameters[6:p]
            ebm_params = parameters[6:p]
            params = read_load_options(bootvar)
            parameters = ebm_params if params is None else params
        yield BootEntry(bootnum, name, partition, path, parameters)


def read_load_options(bootvar: str) -> Optional[str]:
    def read(f, n: int):
        v = f.read(n)
        if len(v) < n:
            raise IOError
        return v
    try:
        with open(FMT_BOOTXXXX_PATH.format(bootvar), 'rb') as f:
            read(f, 8)  # 4 + 4 attributes
            v = read(f, 2)
            file_path_list_length = struct.unpack('=H', v)[0]
            while read(f, 2) != b'\0\0':  # Description
                pass
            read(f, file_path_list_length)
            options = f.read()
        return options.decode('utf-16')
    except Exception:
        return None


class BootView(Gtk.ScrolledWindow):
    def __init__(self):
        super().__init__()

        self.model = model = Gtk.ListStore(str, str, str, str, str)

        tv = Gtk.TreeView(model=model)
        self.add(tv)
        tv.set_grid_lines(Gtk.TreeViewGridLines.BOTH)
        tv.insert_column_with_attributes(-1, _("Bootnum"), Gtk.CellRendererText(), text=0)
        tv.insert_column_with_attributes(-1, _("Name"), Gtk.CellRendererText(), text=1)
        tv.insert_column_with_attributes(-1, _("ESP"), Gtk.CellRendererText(), text=2)
        tv.insert_column_with_attributes(-1, _("Path"), Gtk.CellRendererText(), text=3)
        tv.insert_column_with_attributes(-1, _("Parameters"), Gtk.CellRendererText(), text=4)

        tv.show_all()

        self.accel_group = ag = Gtk.AccelGroup()
        def copy(_ag, window, *_):
            if window.get_focus() != tv:
                return False
            model, it = tv.get_selection().get_selected()
            params = model[it][-1]
            from gi.repository import Gdk
            clipboard = Gtk.Clipboard.get_default(Gdk.Display.get_default())
            clipboard.set_text(params, len(params))
            return True
        key, mods = Gtk.accelerator_parse('<Primary>C')
        ag.connect(key, mods, Gtk.AccelFlags.VISIBLE, copy)

    @staticmethod
    def read_boot_entries(partuuid_map: Mapping[str, str]) -> Iterator[BootEntry]:
        yield from parse_efibootmgr(partuuid_map)

    def append(self, entry: BootEntry) -> None:
        self.model.append([entry.bootnum, entry.name, entry.partition, entry.path, entry.parameters])
