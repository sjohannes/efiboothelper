#!/usr/bin/env python3


from os import path
import setuptools


cwd = path.abspath(path.dirname(__file__))
with open(path.join(cwd, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()


setuptools.setup(
    name='efiboothelper',
    version='0.0.0.dev0',
    description="Show information relevant to editing EFI boot entries",
    long_description=long_description,
    url='https://gitlab.com/sjohannes/efiboothelper',
    author='Johannes Sasongko',
    author_email='sasongko@gmail.com',
    license='GPL-3.0+',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: System :: Boot',
        'Topic :: System :: Hardware',
    ],
    keywords='boot efi partition uefi',
    packages=['efiboothelper'],
    python_requires='>=3.6',
    entry_points={
        'gui_scripts': ['efiboothelper = efiboothelper:main'],
    }
)
